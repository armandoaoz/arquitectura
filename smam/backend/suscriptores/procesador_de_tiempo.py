import sys
sys.path.append('../')
from monitor import Monitor
import pika
from random import randint
import time
import logging


class ProcesadorTiempo:

    medicamentos = ['Paracetamol', 'Insulina', 'Ibuprofeno']
    adultos_m = []
    medicamentos_asig = {}

    def consume(self):
        try:
            logging.basicConfig()
            # Url que define la ubicación del Distribuidor de Mensajes
            url = 'amqp://fbtoekmu:Sz-Lti3ei4EGvSK-9dm_CiAvfWKFt_Ph@eagle.rmq.cloudamqp.com/fbtoekmu'
            # Se utiliza como parámetro la URL dónde se encuentra el Distribuidor
            # de Mensajes
            params = pika.URLParameters(url)
            params.socket_timeout = 5
            # Se establece la conexión con el Distribuidor de Mensajes
            connection = pika.BlockingConnection(params)
            # Se solicita un canal por el cuál se enviarán los signos vitales
            channel = connection.channel()
            # Se declara una cola para leer los mensajes enviados por el
            # Publicador
            channel.queue_declare(queue='time', durable=True)
            channel.basic_qos(prefetch_count=1)
            channel.basic_consume('time', self.callback)
            channel.start_consuming()  # Se realiza la suscripción en el Distribuidor de Mensajes
        except (KeyboardInterrupt, SystemExit):
            channel.close()  # Se cierra la conexión
            sys.exit("Conexión finalizada...")
            time.sleep(1)
            sys.exit("Programa terminado...")

    def callback(self, ch, method, properties, body):
        json_message = self.string_to_json(body)
        self.id_in_list(json_message['id'])
        date_splitter = json_message['date'].split(' ')[4]
        hour_splitter = date_splitter.split(":")
        monitor = Monitor()
        if (hour_splitter[0] == "19" and hour_splitter[1] == "00" or hour_splitter[0] == "00" and hour_splitter[1] == "00") and len(self.medicamentos_asig['Paracetamol']) != 0:
            monitor.print_notification_medicina(json_message['date'], self.medicamentos_asig['Paracetamol'], 'Paracetamol -4.8ml-', json_message['model'])     
        elif(hour_splitter[0] == "20" and hour_splitter[1] == "00" or hour_splitter[0] == "02" and hour_splitter[1] == "00") and len(self.medicamentos_asig['Insulina']) != 0:
            monitor.print_notification_medicina(json_message['date'], self.medicamentos_asig['Insulina'], 'Insulina -100mg/dl-', json_message['model'])     
        elif(hour_splitter[0] == "08" and hour_splitter[1] == "00" or hour_splitter[0] == "14" and hour_splitter[1] == "00") and len(self.medicamentos_asig['Ibuprofeno']) != 0:
            monitor.print_notification_medicina(json_message['date'], self.medicamentos_asig['Ibuprofeno'], 'Ibuprofeno -10ml-', json_message['model'])
        time.sleep(1)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def string_to_json(self, string):
        string = string.decode()
        message = {}
        string = string.replace('{', '')
        string = string.replace('}', '')
        values = string.split(', ')
        for x in values:
            v = x.split(': ')
            message[v[0].replace('\'', '')] = v[1].replace('\'', '')
        return message

    #función que permite comprobar si un id ya se encuentra dentro de una lista
    def id_in_list(self, id):
        if id in self.adultos_m:
            pass
        else:
            self.asignar_medicinas(id)

    #función que asigna las medicinas (de manera aleatoría para usos practicos) a los adultos mayores que están
    #de suscriptores en el momento
    def asignar_medicinas(self, id):
        if bool(self.medicamentos_asig) == False:
            for i in range(len(self.medicamentos)):
                self.medicamentos_asig[self.medicamentos[i]] = []
        if id not in self.adultos_m:
            self.adultos_m.append(id)
            self.medicamentos_asig[self.medicamentos[(randint(0, len(self.medicamentos)-1))]].append(id)
        else:
            pass

if __name__ == '__main__':
    p_tiempo = ProcesadorTiempo()
    p_tiempo.consume()
